"""
b萌自动查票，通知
"""
import os, sys
import requests
import time

def notify(message, title = None, image = None):
    cmd = ["terminal-notifier", "-message", message]
    if title:
        cmd.append("-title")
        cmd.append(title)
    if image:
        cmd.append("-contentImage")
        cmd.append(image)
    os.system(' '.join(cmd))


checked = False
while True:
    minute = time.localtime().tm_min
    if not checked and (minute == 1 or minute == 31):
        result = requests.get("http://bangumi.bilibili.com/moe/2016/2/api/schedule/ranking_86.json").json()['result']
        ch0 = result[0]
        ch1 = result[1]
        notify('"%s %s %s %s"'%(ch0['chn_name'], ch0['ballot_sum'], ch1['chn_name'], ch1['ballot_sum']), 'bilibili')
        checked = True
    else:
        checked = False
    time.sleep(30)
